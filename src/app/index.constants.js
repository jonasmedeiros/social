/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('social')
    .constant('malarkey', malarkey)
    .constant('moment', moment)
    .constant('API_BASE', 'https://logo-social-api.herokuapp.com');
    //.constant('API_BASE', 'http://localhost:8000');

})();
