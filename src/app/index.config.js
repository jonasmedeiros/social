(function() {
  'use strict';

  angular
    .module('social')
    .config(config)
    .config(location)
    .config(auth);

  /** @ngInject */
  function config($logProvider, toastrConfig) {
    // Enable log
    $logProvider.debugEnabled(true);

    // Set options third-party lib
    toastrConfig.allowHtml = true;
    toastrConfig.timeOut = 3000;
    toastrConfig.positionClass = 'toast-top-right';
    toastrConfig.preventDuplicates = true;
    toastrConfig.progressBar = true;
  }

  function location($locationProvider) {
    $locationProvider.html5Mode(true);
  }

  function auth($authProvider) {

    $authProvider.linkedin({
      clientId: 'LinkedIn Client ID'
    });

    $authProvider.yahoo({
      clientId: 'Yahoo Client ID / Consumer Key'
    });

    $authProvider.live({
      clientId: 'Microsoft Client ID'
    });

    // Twitter
    $authProvider.twitter({
      name: 'twitter',
      url: 'https://logo-social-api.herokuapp.com/auth/twitter',
      authorizationEndpoint: 'https://api.twitter.com/oauth/authenticate',
      redirectUri: window.location.origin || window.location.protocol + '//' + window.location.host,
      type: '1.0',
      popupOptions: { width: 495, height: 645 }
    });

    $authProvider.withCredentials = false;

    // No additional setup required for Twitter
  }


})();
