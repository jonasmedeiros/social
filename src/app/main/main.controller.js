(function() {
  'use strict';

  angular
    .module('social')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($http, $auth, FriendsModalService) {

    var vm = this;

    vm.text = "Curabitur nec ligula eu nulla interdum dignissim. Nunc aliquet, elit at cursus rhoncus, orci elit mollis erat, at pulvinar dui nisl eget ligula. Morbi condimentum sollicitudin ipsum, at blandit lacus lacinia eu. Donec consectetur nunc ex, nec mollis turpis tincidunt in. Donec quis justo nunc.";

    vm.authenticate = function(provider) {
      $auth.authenticate(provider)
      .then(function(response) {

        FriendsModalService.init(response, vm.text);

      })
      .catch(function(response) {
        // Something went wrong.
      });
    };

    vm.shareFacebook = function(){
      
      FB.ui({ 
        method: 'send',
        name: 'Logo Website',
        link: 'https://samaretan.com/', 
        description: vm.text, 
        picture: 'https://samaretan.com/img/login_random/background-7.jpg'
      });
    };

  }
})();
