(function () {

  'use strict';

  angular
    .module('social' )
    .service('FriendsModalService', FriendsModalService);

    function FriendsModalService($window, $uibModal){
      
      var modalInstance;
      var flag = false;

      this.init = function(fiends, text){

        if(flag) return false;

        modalInstance = $uibModal.open({
          templateUrl: '/assets/modals/friends.html',
          controller: 'FriendsModalController',
          backdrop  : 'static',
          keyboard: false,
          resolve: {
             Friends: function () {
              return fiends;
             },
             Text: function(){
              return text;
             }
           }
        });

        modalInstance.result.then(function () {
          
        }, function () {
          
        });

        flag = true;

      },

      this.hide = function(){

        if(!flag) return false;

        if(modalInstance){
          modalInstance.close();
        };

        flag = false;
      }
    };
}());