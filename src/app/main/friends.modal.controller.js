(function() {
  'use strict';

  angular
    .module('social')
    .controller('FriendsModalController', FriendsModalController);

  function FriendsModalController($scope, $rootScope, FriendsModalService, Friends, Twitter, Text) {
    
    $scope.sending_runing = false;

    $scope.friends = Friends.data.friends.users;

    $scope.message = Text;

    $scope.users = {
      ids: []
    };

    angular.forEach($scope.friends, function(value) {
      $scope.users.ids.push(value.id);
    });

    $scope.close = function(){
      FriendsModalService.hide();
    }

    $scope.toggle_checkbox = function(id){

      if($scope.users.ids.indexOf(id) === -1){
        $scope.users.ids.push(id);
      }else{
        $scope.users.ids.splice($scope.users.ids.indexOf(id), 1);
      }
    }
    
    $scope.sendMessage = function(){

      var temp = Friends.data.personal_info;
      temp['user_ids'] = $scope.users.ids.toString();
      temp['text'] = $scope.message;

      $scope.sending = "Sending invitations wait please...";

      $scope.sending_runing = true;

      Twitter.post(temp).$promise.then(function(response){
        console.log(response);

        FriendsModalService.hide();
      });
    }

  };

})();
