(function() {
  'use strict';

  angular
    .module('social')
    .factory('Twitter', Twitter);

  function Twitter( $resource, API_BASE ) {

    return $resource(
      API_BASE + '/twitter/send-message',
      {
        id: null
      },
      {
        
        post: {
          method: 'POST',
          isArray: false
        }

      }
    );

  }
})();