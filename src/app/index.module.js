(function() {
  'use strict';

  angular
    .module('social', ['ngAnimate', 
                       'ngCookies', 
                       'ngTouch', 
                       'ngSanitize', 
                       'ngMessages', 
                       'ngAria', 
                       'ngResource', 
                       'ui.router', 
                       'ui.bootstrap', 
                       'toastr',
                       'satellizer',
                       'checklist-model'
                       ]);

})();
