// server.js

// set up ========================
var express        = require('express');
var app            = express();                  // create our app w/ express
var morgan         = require('morgan');          // log requests to the console (express4)
var bodyParser     = require('body-parser');     // pull information from HTML POST (express4)
var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)

app.use(express.static(__dirname + '/dist'));                 // set the static files location /public/img will be /img for users
app.use(morgan('dev'));                                         // log every request to the console
app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json());                                     // parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(methodOverride());

// other configurations etc for express go here...

// configuration =================


// set the port of our application ======================
var port = process.env.PORT || 9000;

// listen (start app with node server.js) ======================================
app.listen(port);
console.log("App listening on port " + port);

// application -------------------------------------------------------------
app.get('/**', function(req, res) {

  // load the single view file (angular will handle the page changes on the front-end)
  res.sendfile('./dist/index.html');
});
